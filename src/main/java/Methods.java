import javax.swing.*;
import java.util.List;
import java.util.Locale;


/**
 * Class containing static utility methods.
 * <p>Not instantiable.</p>
 *
 * @author Leonhard Prechtl
 */
public class Methods {

    /**
     * Not instantiable.
     */
    private Methods(){
	}

	/**
	 * gibt den Index des größten Elements in einem double array zurück
     *
	 * @param array Array in which the larged element shell be searched.
	 * @return Index of the first occurrence of the largest element.
	 */
	public static int findLagestIndexDoubleArray(double[] array) {
		double largest = array[0];
		int index = 0;
		
		for(int i = 0; i < array.length; i++) {
			if(array[i] > largest) {
				largest = array[i];
				index = i;
			}
		}
		return index;
	}

	/**
	 * gibt die Summe aller Elemente in einem float array zurück
     *
	 * @param array Array which shall be summed up.
	 * @return Sum of all array elements
	 */
	public static float sumFloatArray(float[] array) {
		float sum = 0;
		
		for(int i = 0; i < array.length; i++) {
			sum += array[i];
		}
		
		return sum;
	}

	/**
	 * Gui Popup frage nach einer float Zahl
	 *
	 * @param question Text displayed in the dialoge.
	 * @return The value entered by the user.
	 */
	public static float askFloat(String question) {
		String errorMessage = "";
		do {
			String input = JOptionPane.showInputDialog(errorMessage + question);
			errorMessage = "";
			
			try {
				float ask = Float.parseFloat(input);
				return ask;
			}
			catch (NumberFormatException e) {
				errorMessage = "Input must be a number. Use '.' as decimal seperator.\n";
			}
		} while (!errorMessage.isEmpty());

		return 0;
	}

	/**
	 * searches if a subarray[index] in searchList contains the searched string
	 * if so it return the subarray
	 * else it retruns null
	 * @param list <tt>List</tt> of <tt>String[]</tt> which shall be searched.
	 * @param index Index of the String in each String[] in which shall be searched.
	 * @param searched Searched String.
	 * @return The first String[] in list which contains searched.
	 */
	public static String[] searchList(List<String[]> list, int index, String searched) {
		for (String[] array: list){
			if (searched.contains(array[index])) {
				return array;
			}
		}
		return null;
	}

    /**
     * <p>Converts a <tt>double</tt> to a formatted <tt>String</tt>.</p>
     *
     * <p>
     * Examples<br>
     * 123.0d -&gt; "123"<br>
     * 123.456d -&gt; "123.456"<br>
     * 1234567.8d -&gt; "1,234,567.8"
     * </p>
     *
     * @param number Number that shall be converted.
     * @return Number as formatted String.
     */
	public static String doubleToFormattedString(double number) {
        Locale locale = new Locale("en", "US");
		if (number % 1.0 != 0)
			return String.format(locale, "%s", number);
		else
			return String.format(locale, "%,.0f",number);
	}
}
