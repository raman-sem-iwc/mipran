import ij.ImagePlus;
import ij.ImageStack;
import ij.gui.HistogramWindow;
import ij.gui.Overlay;
import ij.gui.Roi;
import ij.io.FileSaver;
import ij.plugin.MontageMaker;
import ij.plugin.filter.EDM;
import ij.plugin.filter.ParticleAnalyzer;
import ij.process.ByteProcessor;
import ij.process.ImageConverter;
import org.apache.commons.cli.*;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.*;
import java.lang.reflect.Array;
import java.util.*;

/**
 * This class contains the main method for the command line version of MipRAn.
 * It reads in the commandline arguments and the config file.
 *
 * <p><b>CURRENTLY NOT USABLE! BROKEN!</b></p>
 *
 * @author Leonhard Prechtl
 */
public class MipRAn_cli {

    public static void main(String[] args) {
//		default location
        String home = "/home/leonhard/Studium/17S/FP_Mikroplasik/Pattern/witek";
//		String home = "D:/Anger/Leo";
        String configPath = "bw.conf";			//configuration file

        Runner_cli runner = new Runner_cli();

//		command line options
        Options options = new Options();
        options.addOption( "b", "black", false, "black particles on white background");
        options.addOption( "w", "white", false,
                "white particles on black background (default)");
        options.addOption("ws","watershed", false, "watershed found particles");
        options.addOption(null, "results", false, "export results as csv");
        options.addOption(null, "mask", false, "export found particles mask");
        options.addOption(null, "coordinates", false,
                "export coordinates as csv for import into witek control");
        options.addOption( Option.builder("bx")
                .longOpt("basex")
                .desc("X coordinate of the center of the image (default: 0)")
                .hasArg()
                .argName("µm")
                .build() );
        options.addOption( Option.builder("by")
                .longOpt("basey")
                .desc("Y coordinate of the center of the image (default: 0)")
                .hasArg()
                .argName("µm")
                .build() );
        options.addOption( Option.builder("c")
                .longOpt("config")
                .desc("config file (default: 'bw.conf')")
                .hasArg()
                .argName("PATH")
                .build() );
        options.addOption("h", "help", false, "print help");
        options.addOption( Option.builder("p")
                .longOpt("protocol")
                .desc("analysation protocol for an objective\n"
                        + "possible values\n"
                        + "'20': 20x objective, dark field\n"
                        + "'20water': value channel, no blur, otsu, watershed"
                        + "'20folder': analyze whole folder"
                        + "'20folderStitching': stitch all image in a folder together and analyze the stitching Image"
                        + "'50': 50x objective, dark field (default)\n"
                        + "'manual': manual selection of the threshold value")
                .hasArg()
                .argName("PROTOCOL")
                .build() );
        options.addOption( Option.builder("s")
                .longOpt("size")
                .desc("minimum size of the particles in pixel (default: 100)")
                .hasArg()
                .argName("SIZE")
                .build() );
        options.addOption( Option.builder("r")
                .longOpt("resolution")
                .desc("resolution in pixels per nm for coordinate export (default: 1.0)")
                .hasArg()
                .argName("RESOLUTION")
                .build() );
        options.addOption( Option.builder("min")
                .longOpt("minSize")
                .desc("minimum particle size in µm")
                .hasArg()
                .argName("SIZE")
                .build() );
        options.addOption( Option.builder("max")
                .longOpt("maxSize")
                .desc("maximum particle size in µm")
                .hasArg()
                .argName("SIZE")
                .build() );

        CommandLineParser parser = new DefaultParser();

//		command line and config file parameters

//		changing of method parameters according to config file and command line parameters

        try {
            CommandLine cmd = parser.parse( options, args);

//			--help:  print help and exit
            if(cmd.hasOption("help")) {
                HelpFormatter formatter = new HelpFormatter();
                formatter.printHelp( "BW [OPTION]", options );
                System.exit(0);
            }

//			--config: config file parameters
            configPath = cmd.getOptionValue("config", "bw.conf");

//			setting method properties according to properties defined in the config file
            try {

//				load config file parameters into properties object
                Properties properties = new Properties();
                BufferedInputStream stream = new BufferedInputStream(new FileInputStream(configPath));
                properties.load(stream);
                stream.close();

//				Set parameter resolution if given to value in config file

                try {
                    String opt = properties.getProperty("resolution");
                    if (opt != null) {
                        runner.setResolution(Float.parseFloat(opt));
                    }
                }
                catch(IllegalArgumentException e) {

                    System.err.println("Invalid resolution in config file");
                }


                try {

                    String opt = properties.getProperty("minPixel");
                    if (opt != null) {
                        runner.setParticleSize(Integer.parseInt(opt));
                    }
                }
                catch(NumberFormatException e) {
                    System.err.println("Invalid minimum size in config file");

                }



                String protocol = properties.getProperty("protocol");
                //			System.out.println(protocol);
                if (Arrays.asList(
                        "20",
                        "20water",
                        "20folder",
                        "20folderStitching",
                        "50",
                        "manual"
                ).contains(properties.getProperty("protocol"))) {
                    runner.setProtocol(properties.getProperty("protocol"));
                }
                else {
                    System.err.println("Invalid protocol '" + protocol + "' in config file");
                }

                try {
                    String opt = properties.getProperty("black");
                    if (opt != null) {
                        runner.setParticlesAreWhite(!Boolean.parseBoolean(opt));
                    }
                }
                catch(NumberFormatException e) {
                    System.err.println("Invalid paticle color in config file");
                }

                try {
                    String opt = properties.getProperty("watershed");
                    if (opt != null) {
                        runner.setWatershed(Boolean.parseBoolean(opt));
                    }
                }
                catch (NumberFormatException e) {
                    System.err.print("Invalid 'watershed' option in config file");
                }

//                home = properties.getProperty("home");


                try {
                    String opt = properties.getProperty("saveResults");
                    if (opt != null) {
                        runner.setSaveRt(Boolean.parseBoolean(opt));
                    }
                }
                catch(NumberFormatException e) {
                    System.err.println("invalid saveResults in config file");
                }
                try {
                    String opt = properties.getProperty("saveMask");
                    if (opt != null) {
                        runner.setSaveMask(Boolean.parseBoolean(opt));
                    }
                }
                catch(NumberFormatException e) {
                    System.err.println("invald saveMask in config file");
                }
                try {
                    String opt = properties.getProperty("exportCoordinates");
                    if (opt != null) {
                        runner.setCoordExport(Boolean.parseBoolean(opt));
                    }
                }
                catch(NumberFormatException e) {
                    System.err.println("invald exportCoordinates in config file");
                }

                try {
                    String opt = properties.getProperty("basex");
                    if (opt != null) {
                        runner.setCoordinateBaseX(Double.parseDouble(opt));
                    }
                }
                catch(NumberFormatException e) {
                    System.err.println("invalid basex in config file");
                }
                try {
                    String opt = properties.getProperty("basey");
                    if (opt != null) {
                        runner.setCoordinateBaseY(Double.parseDouble(opt));
                    }
                }
                catch(NumberFormatException e) {
                    System.err.println("invalid basey in config file");
                }

                try {
                    String opt = properties.getProperty("minSize");
                    if (opt != null) {
                        runner.setMinSize(Double.parseDouble(opt));
                    }
                }
                catch(NumberFormatException e) {
                    System.err.println("invalid minSize in config file");
                }

                try {
                    String opt = properties.getProperty("maxSize");
                    if (opt != null) {
                        runner.setMaxSize(Double.parseDouble(opt));
                    }
                }
                catch(NumberFormatException e) {
                    System.err.println("invalid maxSize in config file");
                }
            }
            catch (IOException e) {
                e.printStackTrace();
            }

//			setting method properties according to command line options
//			if a method property defined by the config file are overridden if also defined in the command line

//			--black:  particle color
            if(cmd.hasOption("black")) {
                runner.setParticlesAreWhite(false);
            }
            else if (cmd.hasOption("white")) {
                runner.setParticlesAreWhite(true);
            }

//			--watershed: watershed found particles
            if (cmd.hasOption("watershed") ) {
                runner.setWatershed(true);
            }

//			export data
            if(cmd.hasOption("results")) {
                runner.setSaveRt(true);
            }
            if(cmd.hasOption("mask")) {
                runner.setSaveMask(true);
            }
            if(cmd.hasOption("coordinates")) {
                runner.setCoordExport(true);
            }


//			--size:  set minimum particle size
            if (cmd.hasOption("size")) {
                String optionSize = cmd.getOptionValue("size");
                try {
                    runner.setParticleSize(Integer.parseInt(optionSize));
                }
                catch(NumberFormatException e1) {
                    System.err.println("Invalid number '" + optionSize + "' for size");
                }
            }

//			--protocol:  possible measurement protocols
            if (cmd.hasOption("protocol")) {
                if (Arrays.asList(
                        "20",
                        "20water",
                        "20folder",
                        "20folderStitching",
                        "50",
                        "manual"
                ).contains(cmd.getOptionValue("protocol"))) {
                    runner.setProtocol(cmd.getOptionValue("protocol"));
                }
                else {
                    System.err.println("Invalid protocol '" + cmd.getOptionValue("protocol") + "'");
                }
            }


//			--zoom:  zoom factor in pixels per µm
            if (cmd.hasOption("resolution")) {
                String optionResolution = cmd.getOptionValue("resolution");
                try {
                    runner.setResolution(Float.parseFloat(optionResolution));
                }
                catch(NumberFormatException e1) {
                    System.err.println("Invalid number '" + optionResolution + "' for zoom");
                }
            }

//			--base: coordinate base (image center)
            if (cmd.hasOption("basex")) {
                String optionBase = cmd.getOptionValue("basex");
                try {
                    runner.setCoordinateBaseX(Double.parseDouble(optionBase));
                }
                catch (NumberFormatException e1) {
                    System.err.println("Invalid number '" + optionBase + "' for coordinate base X");
                }
            }
            if (cmd.hasOption("basey")) {
                String optionBase = cmd.getOptionValue("basey");
                try {
                    runner.setCoordinateBaseY(Double.parseDouble(optionBase));
                }
                catch (NumberFormatException e1) {
                    System.err.println("Invalid number '" + optionBase + "' for coordinate base Y");
                }
            }

            if (cmd.hasOption("minSize")) {
                String optionBase = cmd.getOptionValue("minSize");
                try {
                    runner.setMinSize(Double.parseDouble(optionBase));
                }
                catch (NumberFormatException e1){
                    System.err.println("Invalid number '" + optionBase + "' for minSize");
                }
            }
            if (cmd.hasOption("maxSize")) {
                String optionBase = cmd.getOptionValue("maxSize");
                try {
                    runner.setMaxSize(Double.parseDouble(optionBase));
                }
                catch (NumberFormatException e1){
                    System.err.println("Invalid number '" + optionBase + "' for maxSize");
                }
            }


        } catch(ParseException e) {
            System.err.println("Fehler " + e.getMessage() );
            System.exit(1);
        }

        runner.run();


////		ImagePlus imporg = imp.duplicate();
////		imporg.getProcessor().blurGaussian(40);
////		imporg.show();
//
//
////		float blurGaussianSigma = (askFloat("Gaussian blur sigma?"));
////		imp.show();
//
////		String[] colors = new String[2];
////		colors[0] = "white";
////		colors[1] = "black";
////		int colorSelection = JOptionPane.showOptionDialog(null, "Which color do the particles have?", "Particle color?",
////				0, JOptionPane.INFORMATION_MESSAGE, null, colors, null);
////
////		boolean particlesAreWhite = true;
////		if (colorSelection == 1) {
////			particlesAreWhite = false;
////		}
////		else if (colorSelection == 0) {
////			particlesAreWhite = true;
////		}
//
////		double blurGaussianSigma = autoBlurDiff(imp, 10, 1, particlesAreWhite);
////
////		System.out.println(blurGaussianSigma);
////
////		imp.getProcessor().blurGaussian(blurGaussianSigma);
//
//        ImagePlus imp = null;
//
//        ResultsTableExtra rt = new ResultsTableExtra();
//
//        int rtOptions =
////				ij.plugin.filter.ParticleAnalyzer.DISPLAY_SUMMARY +
////				ij.plugin.filter.ParticleAnalyzer.SHOW_RESULTS +
////				ij.plugin.filter.ParticleAnalyzer.SHOW_MASKS +
//                ij.plugin.filter.ParticleAnalyzer.SHOW_OVERLAY_MASKS
////				ij.plugin.filter.ParticleAnalyzer.SHOW_OVERLAY_OUTLINES
////				ij.plugin.filter.ParticleAnalyzer.INCLUDE_HOLES
////				ij.plugin.filter.ParticleAnalyzer.SHOW_OUTLINES
//                ;
//
//
//
//        ParticleAnalyzer analy = new ParticleAnalyzer(rtOptions, 0, rt, particleSize, 10000000);
//
//
//
//        if (protocol.equals("20")) {
//            imp = analyze20(home, particlesAreWhite, particleSize, watershed, saveMask, coordExport, resolution,
//                    coordinateBaseX, coordinateBaseY);
//        }
//        else if (protocol.equals("20water")) {
////			open image to be analyzed with a graphical file chooser
//            imp = imageOpener(home);
//
////			convert image into grayscale
//            ImageConverter conv = new ImageConverter(imp);
//            conv.convertToHSB();
//
////			get value image out of HSV channel stack
//            ImagePlus impV = new ImagePlus("Value", imp.getStack().getProcessor(3));
//
////			autothreshold image
//            impV.getProcessor().setAutoThreshold(ij.process.AutoThresholder.Method.Otsu, particlesAreWhite);
//
//            if (particlesAreWhite == true) {
//                System.out.println(impV.getProcessor().getMinThreshold());
//            }
//            else {
//                System.out.println(impV.getProcessor().getMaxThreshold());
//            }
//
////			Find particles in Thresholded image
////			Neccesary creates Particle mask as overlay
//            analy.analyze(impV);
//
////			Create new image out of overlay
//            ImagePlus impThres = new ImagePlus("Threshold image",
//                    new ByteProcessor(impV.getProcessor().getWidth(), impV.getProcessor().getHeight()));
//            impThres.setOverlay(impV.getOverlay());
//            impThres.getOverlay().setFillColor(java.awt.Color.WHITE);
//            impThres.getOverlay().drawLabels(false);
//            impThres = impThres.flatten();
//            impThres.setProcessor(impThres.getProcessor().convertToByteProcessor());
//
////			watershed thresholded image
//            EDM distance = new EDM();
//            distance.toWatershed(impThres.getProcessor());
//
//
////			find particles in watershed image
//
//            int rtOptionsWatershed =
////			ij.plugin.filter.ParticleAnalyzer.DISPLAY_SUMMARY +
////			ij.plugin.filter.ParticleAnalyzer.SHOW_RESULTS +
////			ij.plugin.filter.ParticleAnalyzer.SHOW_MASKS +
////			ij.plugin.filter.ParticleAnalyzer.SHOW_OVERLAY_MASKS
//                    ij.plugin.filter.ParticleAnalyzer.SHOW_OVERLAY_OUTLINES
////			ij.plugin.filter.ParticleAnalyzer.INCLUDE_HOLES
////			ij.plugin.filter.ParticleAnalyzer.SHOW_OUTLINES
//                    ;
//
//            int measurementsWatershed =
//                    ij.measure.Measurements.AREA +
//                            ij.measure.Measurements.CENTROID +
//                            ij.measure.Measurements.FERET
////			ij.measure.Measurements.SHAPE_DESCRIPTORS
//                    ;
//
//            ParticleAnalyzer analyWater = new ParticleAnalyzer(
//                    rtOptionsWatershed, measurementsWatershed, rt, particleSize, 10000000);
//            analyWater.analyze(impThres);
//
//            impThres.show();
//
////			rt.show("impThres");
//        }
//
//        else if (protocol.equals("20folder") ) {
//            analyze20Folder(home, particleSize, watershed, coordExport, resolution, coordinateBaseX, coordinateBaseY);
//        }
//
//        else if (protocol.equals("20folderStitching") ) {
//            imp = analyze20FolderStitching(home, particlesAreWhite, particleSize, watershed, saveMask,
//                    coordExport, resolution, coordinateBaseX, coordinateBaseY);
//        }
//
//        else if (protocol.equals("50")) {
////			open image to be analyzed with a graphical file chooser
//            imp = imageOpener(home);
//
////			convert image into grayscale
//            ImageConverter conv = new ImageConverter(imp);
//            conv.convertToGray8();
//
//
////			ImagePlus imporg = imp.duplicate();
//
//            imp.getProcessor().blurGaussian(2);
//            imp.getProcessor().setAutoThreshold(ij.process.AutoThresholder.Method.Otsu, particlesAreWhite);
//
////			ImagePlus trenn = Watershed.computeWatershed(imporg, imp, 8);
////			trenn.show();
//
//            analy.analyze(imp);
//        }
//
////		manual selection of threshold value with the help of the image histogram
//        else if (protocol.equals("manual")) {
////			open image to be analyzed with a graphical file chooser
//            imp = imageOpener(home);
//
////			convert image into grayscale
//            ImageConverter conv = new ImageConverter(imp);
//            conv.convertToHSB();
//            imp.setProcessor(imp.getStack().getProcessor(3));
//
//
////			show histogram
//            HistogramWindow hw = new HistogramWindow(imp);
//            hw.showHistogram(imp, 256);
//            hw.setVisible(true);
//
////			ask for threshold value
//            int threshold = 140;
////
////			while (threshold < 0) {
////				String thresholdInput;
////				thresholdInput = JOptionPane.showInputDialog(null, "Theshold value? (range: 0-255)", null);
////
////				try {
////					Integer.valueOf(thresholdInput);
////				}
////				catch (NumberFormatException e) {
////					JOptionPane.showMessageDialog(null, "'" + thresholdInput + "' is not a number", "Input Error", JOptionPane.ERROR_MESSAGE);
////
////				}
////			}
//
//            imp.getProcessor().setThreshold(0, threshold, ij.process.ImageProcessor.BLACK_AND_WHITE_LUT);
//
//            analy.analyze(imp);
//
//        }
//
//
//
//
//
//
////		ImagePlus trenn = Watershed.computeWatershed(imporg, imp, 8);
////
////		trenn.show();
//
//
//
//
//
//
////		Calculate feret diameter in µm and list it in column umFeret
//
//        try {
//            rt.convertToUm("Feret", resolution);
//            rt.deleteRowsExtrema("umFeret", minSize, maxSize);
//        }
//        catch (IllegalArgumentException e) {
//
//        }
//
//
////		delete rows in Resultstable where particles are to big o to small
//
//
//
////		rt.show("rt");
//
////		imporg.show();
//
////		rt.show("Particles found");
////		int saveRt = JOptionPane.showConfirmDialog(null, "Save results table as csv?\n(Not suitible for import into WITec control software)","Save results?",JOptionPane.YES_NO_OPTION);
////		if (saveRt == JOptionPane.YES_OPTION){
////			ResultsTableExtra.rtSaver(rt, home);
////		}
//
////		save reults table as .csv
//        if (saveRt == true) {
//            rt.rtSaver(home);
//        }
//
//////		save mask of found particles as .bmp
////		if (saveMask == true) {
////			ImagePlus over = analy.getOutputImage();
////
////			try {
////				FileSaver save = new FileSaver(over);
////				save.saveAsBmp();
////			} catch (NullPointerException e){
////				e.printStackTrace();
////			}
////		}
//
////		ij.WindowManager.getCurrentWindow().close();
//
////		int saveMask = JOptionPane.showConfirmDialog(null, "Save found particles mask?","Save mask?",JOptionPane.YES_NO_OPTION);
////
////		if (saveMask == JOptionPane.YES_OPTION){
////			ImagePlus over = analy.getOutputImage();
////
////			try {
////				FileSaver save = new FileSaver(over);
////				save.saveAsBmp();
////			} catch (NullPointerException e){
////				e.printStackTrace();
////			}
////		}
//
//
////		export coordinates as .csv for witek control software
////		coordinateBase: coordinates of the image center in the witek control software
////		if (coordExport == true) {
////			rt.coordExport(home, imp.getProcessor().getHeight(), resolution, coordinateBaseX, coordinateBaseY);
////		}
//
//
////		int exportCoord = JOptionPane.showConfirmDialog(null, "Export csv with coordinates for import into WITec control software?","Export coordinates?",JOptionPane.YES_NO_OPTION);
////		if (exportCoord == JOptionPane.YES_OPTION){
////			float resolution = Methods.askFloat("Pixels per µm?");
////			ResultsTableExtra.coordExport(rt, home, imp.getProcessor().getHeight(), resolution);
////		}
//
//
//        System.out.println("ran without error");
    }
//
//    //	open an image from file
////	only .bmp are excepted
////    public static ImagePlus imageOpener(String home) {
////
////        JFileChooser chooser = new JFileChooser(home);
////
////        chooser.addChoosableFileFilter(new FileNameExtensionFilter("BMP Images", "bmp"));
////        chooser.addChoosableFileFilter(new FileNameExtensionFilter("PNG Images", ".png"));
////        int returnVal = chooser.showOpenDialog(null);
////
////        ImagePlus imp = null;
////
////        if(returnVal == JFileChooser.APPROVE_OPTION) {
////            imp  = ij.IJ.openImage(chooser.getSelectedFile().getAbsolutePath());
////        }
////
////        return imp;
////    }
////
////    public static ArrayList<ImagePlusPath> folderOpener(String home, boolean subfolders) {
////        JFileChooser chooser = new JFileChooser(home);
////
////        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
////
////        int returnVal = chooser.showOpenDialog(null);
////
////        if(returnVal != JFileChooser.APPROVE_OPTION) {
////            System.err.println("Folder not found");
////            System.exit(1);
////        }
////
////        File folder = new File(chooser.getSelectedFile().getAbsolutePath());
////
////        FilenameFilter imageFilter = new FilenameFilter() {
////
////            @Override
////            public boolean accept(File dir, String name) {
////
////                if (name.toLowerCase().endsWith(".png") ||
////                        name.toLowerCase().endsWith(".bmp")) {
////                    return true;
////                }
////                else if (subfolders) {
////                    return dir.isDirectory();
////                }
////                else {
////                    return false;
////                }
////            }
////        };
////
////        ArrayList<File> imagesList = listFiles(folder, imageFilter);
////
////        ArrayList<ImagePlusPath> impArray = new ArrayList<ImagePlusPath>(0);
////
////        for (File image : imagesList) {
////            if (image.isFile()) {
//////				System.out.println("F: " + image.getAbsolutePath());
////                ImagePlus imp = ij.IJ.openImage(image.getAbsolutePath());
////                File path = new File(image.getAbsolutePath());
////                ImagePlusPath impp = new ImagePlusPath(imp, path);
////                impArray.add(impp);
////            }
////        }
////
////        if (impArray.isEmpty()) {
////            System.err.println("Folder contains no images");
////            System.exit(1);
////        }
////
////        return impArray;
////    }
////
////    public static ArrayList<File> listFiles (File folder, FilenameFilter filter) {
////        File[] fileList = folder.listFiles(filter);
////
////        ArrayList<File> returnFiles = new ArrayList<File>(0);
////
//////		System.out.println(fileList);
////
////        for (File file : fileList) {
//////			System.out.println("F: " + file.getAbsolutePath());
////            if (file.isFile()) {
////                returnFiles.add(file);
////            }
////            else if (file.isDirectory()) {
////
////                returnFiles.addAll(listFiles(file, filter));
////            }
////        }
////        return returnFiles;
////    }
//
//    //	TODO: non static
//    public static ImagePlus analyze20 (String home, boolean particlesAreWhite, int particleSize, boolean watershed,
//                                       boolean saveMask, boolean coordExport, float resolution, double coordinateBaseX,
//                                       double coordinateBaseY) {
////		open image to be analyzed with a graphical file chooser
//        ImagePlus imp = imageOpener(home);
//
//        ImagePlus impV = analyze20Backend(imp, particlesAreWhite, particleSize, watershed, coordExport, home,
//                resolution, coordinateBaseX, coordinateBaseY);
//
//        impV.show();
//
//        impV = impV.flatten();
//
////		save mask of found particles as .bmp
//        if (saveMask == true) {
//            try {
//                FileSaver save = new FileSaver(impV);
//                save.saveAsBmp();
//            } catch (NullPointerException e){
//                e.printStackTrace();
//            }
//        }
//
//
//
//        return impV;
//    }
//
//    //	TODO: non static
//    public static ImagePlus analyze20Backend(ImagePlus imp, boolean particlesWhite, int particleSize,
//                                             boolean watershed, boolean coordExport, String home, float resolution,
//                                             double coordinateBaseX, double coordinateBaseY) {
//
//        ImagePlus imporg = (ImagePlus) imp.clone();
//
////		convert image into grayscale
//        ImageConverter conv = new ImageConverter(imp);
//        conv.convertToHSB();
//
////		get value image out of HSV channel stack
//        ImagePlus impV = new ImagePlus("Value", imp.getStack().getProcessor(3));
//
////		autothreshold image
//        impV.getProcessor().setAutoThreshold(ij.process.AutoThresholder.Method.Otsu, particlesWhite);
//
////		if (particlesWhite) {
////			System.out.println(impV.getProcessor().getMinThreshold());
////		}
////		else {
////			System.out.println(impV.getProcessor().getMaxThreshold());
////		}
//
////		impV.show();
//
////		if the image should be watersheded the calculations are done in this if clause
////		TODO: create a new method for watershedding, which takes the thresholded image and returns the watersheded thresholded image
//        if (watershed) {
////			Find particles in Thresholded image
////			Neccesary creates Particle mask as overlay
//            int rtOptions =
////					ij.plugin.filter.ParticleAnalyzer.DISPLAY_SUMMARY +
////					ij.plugin.filter.ParticleAnalyzer.SHOW_RESULTS +
////					ij.plugin.filter.ParticleAnalyzer.SHOW_MASKS +
//                    ij.plugin.filter.ParticleAnalyzer.SHOW_OVERLAY_MASKS
////					ij.plugin.filter.ParticleAnalyzer.SHOW_OVERLAY_OUTLINES
////					ij.plugin.filter.ParticleAnalyzer.INCLUDE_HOLES
////					ij.plugin.filter.ParticleAnalyzer.SHOW_OUTLINES
//                    ;
//
//            ResultsTableExtra rtWat = new ResultsTableExtra();
//            ParticleAnalyzer analyWat = new ParticleAnalyzer(rtOptions, 0, rtWat, particleSize, 10000000);
//            analyWat.analyze(impV);
//
////			Create new image out of overlay
//            ImagePlus impThres = new ImagePlus("Threshold image",
//                    new ByteProcessor(impV.getProcessor().getWidth(), impV.getProcessor().getHeight()));
//            impThres.setOverlay(impV.getOverlay());
//            if (impThres.getOverlay() != null) {
//                impThres.getOverlay().setFillColor(java.awt.Color.WHITE);
//                impThres.getOverlay().drawLabels(false);
//            }
//            impThres = impThres.flatten();
//            impThres.setProcessor(impThres.getProcessor().convertToByteProcessor());
//
////			watershed thresholded image
//            EDM distance = new EDM();
//            distance.toWatershed(impThres.getProcessor());
//
////			create a threshold overlay, as image is binary (0 and 255) the exact lower threshold value (50) does not matter and the upper threshold value has to bee 255.
//            impThres.getProcessor().setThreshold(50, 255, ij.process.ImageProcessor.RED_LUT);
//
//
//
//            impV = (ImagePlus) impThres.clone();
//        }
//
//        ResultsTableExtra rt = new ResultsTableExtra();
//
//        int rtOptions =
////				ij.plugin.filter.ParticleAnalyzer.DISPLAY_SUMMARY +
////				ij.plugin.filter.ParticleAnalyzer.SHOW_RESULTS +
////				ij.plugin.filter.ParticleAnalyzer.SHOW_MASKS +
////				ij.plugin.filter.ParticleAnalyzer.SHOW_OVERLAY_MASKS +
//                ij.plugin.filter.ParticleAnalyzer.SHOW_OVERLAY_OUTLINES
////				ij.plugin.filter.ParticleAnalyzer.INCLUDE_HOLES
////				ij.plugin.filter.ParticleAnalyzer.SHOW_OUTLINES
//                ;
//
//        int measurements =
//                ij.measure.Measurements.AREA +
//                        ij.measure.Measurements.CENTROID +
//                        ij.measure.Measurements.FERET
////				ij.measure.Measurements.SHAPE_DESCRIPTORS
//                ;
//
//        ParticleAnalyzer analy = new ParticleAnalyzer(rtOptions, measurements, rt, particleSize, 10000000);
//
//
////		find particles in thresholded image
//        analy.analyze(impV);
//
////		impV.show();
//
////		export coordinates as .csv for witek control software
////		coordinateBase: coordinates of the image center in the witek control software
//        if (coordExport == true) {
//            rt.coordExport(home, imp.getProcessor().getHeight(), resolution, coordinateBaseX, coordinateBaseY);
//        }
//
////		Remove Threshold drawing out of Image
//        impV.getProcessor().resetThreshold();
//
////		Draw particle outlines green and thicker
//        Overlay overlay = impV.getOverlay();
//
////		skip if no particles found
//        if (overlay != null ) {
//            overlay.setStrokeColor(java.awt.Color.GREEN);
//            int num = 0;
//            for (int i=0; i<overlay.size(); i++) {
//                Roi roi = overlay.get(i);
//                roi.setStrokeWidth(5.0);
//                num++;
//            }
//            System.out.println(num);
//        }
//        else {
//            System.out.println("no particles found");
//        }
//
//        imporg.setOverlay(overlay);
//
//        return imporg;
//    }
//
//    public static void analyze20Folder(String home, int particleSize, boolean watershed, boolean coordExport,
//                                       float resolution, double coordinateBaseX, double coordinateBaseY) {
//
//        ArrayList<ImagePlusPath> impArray = folderOpener(home, true);
//
//        for (ImagePlusPath imp : impArray) {
//            boolean particlesWhite = true;
//            if (imp.getPath().getName().contains("BF")) {
//                particlesWhite = false;
//            }
//
//            if (imp.getPath().getName().contains("_Overlay")) {
//                continue;
//            }
//
//            ImagePlus impV = analyze20Backend(imp.getImagePlus(), particlesWhite, particleSize, watershed,
//                    coordExport, home, resolution, coordinateBaseX, coordinateBaseY);
//
//            String name = imp.getPath().getName();
//            int lastDot = name.lastIndexOf('.');
//            String path = imp.getPath().getParent()
//                    + File.separator
//                    + name.substring(0, lastDot) + "_Overlay" + name.substring(lastDot);
//
//            System.out.println(path);
//
//            try {
//                FileSaver save = new FileSaver(impV);
//                save.saveAsPng(path);
//            } catch (NullPointerException e){
//                e.printStackTrace();
//            }
//
//        }
//
//
//
//    }
//
//    public static ImagePlus analyze20FolderStitching(String home, boolean particlesAreWhite, int particleSize,
//                                                     boolean watershed, boolean saveMask, boolean coordExport,
//                                                     float resolution, double coordinateBaseX, double coordinateBaseY) {
//
//        int columns;
//        int rows;
//
//        ArrayList<ImagePlusPath> impArray = folderOpener(home, false);
//
//        Collections.sort(impArray, new Comparator<ImagePlusPath>() {
//            @Override
//            public int compare(ImagePlusPath impP1, ImagePlusPath impP2)
//            {
//                String path1 = impP1.getPath().toString();
//                String path2 = impP2.getPath().toString();
//
//                int rowIndex1 = path1.lastIndexOf('-');
//                int rowIndex2 = path2.lastIndexOf('-');
//
//                int columnIndex1 = path1.lastIndexOf('-', rowIndex1 - 1);
//                int columnIndex2 = path2.lastIndexOf('-', rowIndex2 - 1);
//
//                String numStr1 = path1.substring(rowIndex1 + 1, rowIndex1 + 4)
//                        + path1.substring(columnIndex1 + 1, columnIndex1 + 4);
//                String numStr2 = path2.substring(rowIndex2 + 1, rowIndex2 + 4)
//                        + path2.substring(columnIndex2 + 1, columnIndex2 + 4);
//
////				 	int num1 = Integer.parseInt(numStr1);
////				 	int num2 = Integer.parseInt(numStr2);
//
//
//                return numStr1.compareTo(numStr2);
//            }
//        });
//
//        String lastPath = impArray.get(impArray.size() - 1).getPath().toString();
//        int rowIndex = lastPath.lastIndexOf('-');
//        int columnIndex = lastPath.lastIndexOf('-', rowIndex - 1);
//        columns = Integer.parseInt(lastPath.substring(columnIndex + 1, columnIndex + 4)) + 1;
//        rows = Integer.parseInt(lastPath.substring(rowIndex + 1, rowIndex + 4)) + 1;
//
//
//        ImageStack stack = new ImageStack(
//                impArray.get(1).getImagePlus().getWidth(), impArray.get(1).getImagePlus().getHeight());
//        for (ImagePlusPath imp : impArray) {
//            stack.addSlice(imp.getImagePlus().getProcessor());
//        }
//
//        ImagePlus impStack = new ImagePlus("Stack", stack);
//
//        MontageMaker montage = new MontageMaker();
//        ImagePlus stitched = montage.makeMontage2(impStack, columns, rows, 1, 1, stack.getHeight(),
//                1, 0, false);
//
//        ImagePlus impV = analyze20Backend(stitched, particlesAreWhite, particleSize, watershed, coordExport,
//                home, resolution, coordinateBaseX, coordinateBaseY);
//
////		impV.show();
//
//        impV = impV.flatten();
//
////		save mask of found particles as .bmp
//        if (saveMask == true) {
//            try {
//                FileSaver save = new FileSaver(impV);
//                save.saveAsBmp();
//            } catch (NullPointerException e){
//                e.printStackTrace();
//            }
//        }
//
//        return impV;
//
//
////			ImagePlus impV = analyze20Backend(imp.getImagePlus(), particlesWhite, particleSize, watershed, coordExport, home, resolution, coordinateBaseX, coordinateBaseY);
////
////			String name = imp.getPath().getName();
////			int lastDot = name.lastIndexOf('.');
////			String path = imp.getPath().getParent() + File.separator + name.substring(0, lastDot) + "_Overlay" + name.substring(lastDot);
////
////			System.out.println(path);
////
////			try {
////				FileSaver save = new FileSaver(impV);
////				save.saveAsPng(path);
////			} catch (NullPointerException e){
////				e.printStackTrace();
////			}
//
//    }
//
//
//
//
////	public static ImagePlus watershed(ImagePlus imp) {
//////		get B/W thresholded image
////		ImagePlus impThres = analy.getOutputImage();
////
//////		impThres.show();
////
//////		watershed thresholded image
////		EDM distance = new EDM();
////		distance.toWatershed(impThres.getProcessor());
////
//////		find particles in watershed image
////		analy.analyze(impThres);
////	}
//
//
//
//////	Methode um Ideale unschärfe des Bildes festzulegen
//////	Funktioniert nicht gut
////	public static double autoBlurDiff(ImagePlus ima, double max, double step, boolean particlesWhite) {
////		ImagePlus[] blured = new ImagePlus[(int) (max/step)];
////		double[] allAreas = new double[(int) (max/step)];
////		double[] change = new double[(int) (max/step)];
////
////		for(float i = 0; i < max; i+=step) {
//////			System.out.println(i);
////
////			blured[(int) (i/step)] = ima.duplicate();
////			blured[(int) (i/step)].getProcessor().blurGaussian(i);
////			blured[(int) (i/step)].getProcessor().setAutoThreshold(ij.process.AutoThresholder.Method.Triangle, particlesWhite);
////
////			ResultsTable rst = new ResultsTable();
////
////			int options =
//////					ij.plugin.filter.ParticleAnalyzer.DISPLAY_SUMMARY +
//////					ij.plugin.filter.ParticleAnalyzer.SHOW_RESULTS +
//////					ij.plugin.filter.ParticleAnalyzer.SHOW_MASKS +
////					ij.plugin.filter.ParticleAnalyzer.INCLUDE_HOLES
////					;
////
////			int measurements =
////					ij.measure.Measurements.AREA
//////					ij.measure.Measurements.CENTROID
//////					ij.measure.Measurements.SHAPE_DESCRIPTORS
////					;
////
////			ParticleAnalyzer analy = new ParticleAnalyzer(options, measurements, rst, 100, 10000000);
////			analy.analyze(blured[(int) (i/step)]);
////
////			float[] area = rst.getColumn(rst.getColumnIndex("Area"));
////			float areaSum = Methods.sumFloatArray(area);
////			System.out.println(areaSum);
////
////			allAreas[(int) (i/step)] = areaSum;
////		}
////
////		change[0] = 0;
////		for(double i = 1; i < max/step; i++){
////			change[(int) i] = allAreas[(int) i] / allAreas[(int) (i-1)] - 1;
////			System.out.println(change[(int) i]);
////		}
////		double largestChange = (int) (Methods.findLagestIndexDoubleArray(change)*step);
////		return largestChange;
////	}
////
////
}
