import com.opencsv.CSVWriter;
import ij.ImagePlus;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Class containing static file handling utility methods.
 * Not instantiable.
 *
 * @author Leonhard Prechtl
 */
public final class Files {
    /**
     * Not instantiable.
     */
    private Files(){
    }

    /**
     * Opens a single image from file using a graphical file chooser
     *
     * @param home Default folder of the file chooser.
     * @return opended image if successful
     *         else null
     */
    public static ImagePlus imageOpener(String home) {

        JFileChooser chooser = new JFileChooser(home);

        chooser.setFileFilter(new FileNameExtensionFilter("Images '.png', '.bmp'", "png", "bmp"));

        int returnVal = chooser.showOpenDialog(null);

        ImagePlus imp = null;

        if(returnVal == JFileChooser.APPROVE_OPTION) {
            imp  = ij.IJ.openImage(chooser.getSelectedFile().getAbsolutePath());
        }

        return imp;
    }

    /**
     * Chooses a folder using a graphical file chooser.
     *
     * @param home Default folder of the file chooser
     * @return chosen folder; null if canceled
     */
    public static File folderChooser(String home){
        JFileChooser chooser = new JFileChooser(home);

        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

        int returnVal = chooser.showOpenDialog(null);

        File folder = null;

        if(returnVal == JFileChooser.APPROVE_OPTION) {
            folder = new File(chooser.getSelectedFile().getAbsolutePath());
        }


        return folder;
    }

    /**
     * Reads all images ('.png', '.bmp') in the given folder into an <tt>ArrayList</tt>
     *
     * @param folder The folder which contains the images.
     * @param subfolders <tt>true</tt> Subfolders are also imported (recursive)
     * @return all found images
     */
    public static ArrayList<ImagePlusPath> folderImageOpener(File folder, boolean subfolders) {


        FilenameFilter imageFilter = new FilenameFilter() {

            @Override
            public boolean accept(File dir, String name) {

                if (name.toLowerCase().endsWith(".png") ||
                        name.toLowerCase().endsWith(".bmp")) {
                    return true;
                }
                else if (subfolders) {
                    return dir.isDirectory();
                }
                else {
                    return false;
                }
            }
        };

        ArrayList<File> imagesList = listFiles(folder, imageFilter);

        ArrayList<ImagePlusPath> impArray = new ArrayList<ImagePlusPath>(0);

        for (File image : imagesList) {
            if (image.isFile()) {
//				System.out.println("F: " + image.getAbsolutePath());
                ImagePlus imp = ij.IJ.openImage(image.getAbsolutePath());
                File path = new File(image.getAbsolutePath());
                ImagePlusPath impp = new ImagePlusPath(imp, path);
                impArray.add(impp);
            }
        }

        if (impArray.isEmpty()) {
            System.err.println("Folder contains no images");
            System.exit(1);
        }

        return impArray;
    }

    public static List<String[]> folderInfoOpener(File folder) {
        FilenameFilter imageFilter = new FilenameFilter() {

            @Override
            public boolean accept(File dir, String name) {

                if (name.toLowerCase().endsWith(".csv")) {
                    return true;
                }
                else {
                    return false;
                }
            }
        };

        ArrayList<File> csvFilesList = listFiles(folder, imageFilter);

        List<String[]> infos = new ArrayList<>();


        for (File path : csvFilesList) {
            if (path.isFile()) {
                String line = "";
                String cvsSplitBy = ",";

                try (BufferedReader br = new BufferedReader(new FileReader(path))) {
                    while ((line = br.readLine()) != null) {
                        infos.add(line.split(cvsSplitBy));
                    }
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            }
        }

        if (csvFilesList.isEmpty()) {
            System.err.println("Folder contains no .csv File");
            System.exit(1);
        }

        return infos;
    }

    public static ArrayList<File> listFiles (File folder, FilenameFilter filter) {
        File[] fileList = folder.listFiles(filter);

        ArrayList<File> returnFiles = new ArrayList<File>(0);

//		System.out.println(fileList);

        for (File file : fileList) {
//			System.out.println("F: " + file.getAbsolutePath());
            if (file.isFile()) {
                returnFiles.add(file);
            }
            else if (file.isDirectory()) {

                returnFiles.addAll(listFiles(file, filter));
            }
        }
        return returnFiles;
    }

    /**
     * Exports a <tt>List</tt> of <tt>String[]</tt> with coordinates into a .csv file using a graphical file chooser.
     *
     *
     * @param coordList List of coordinates.
     *                  Each List element should contain one particle
     *                  String[0] its number
     *                  String[1] X coordinate
     *                  String[2] Y coordinate
     * @param home Default folder of the file chooser
     */
    public static void saveCoordListCSV(List<String[]> coordList, String home) {
        //		Erstellen der zu speichernden Datei
        JFileChooser chooser = new JFileChooser(home);

        FileNameExtensionFilter filter = new FileNameExtensionFilter("CSV files '.csv'", "csv");
        chooser.setFileFilter(filter);
        int returnVal = chooser.showSaveDialog(null);


        if(returnVal == JFileChooser.APPROVE_OPTION) {
            String path = chooser.getSelectedFile().getAbsolutePath();
            String extension = path.substring(path.lastIndexOf(".") + 1, path.length());

            if (!extension.equals("csv")) {
                path = path + ".csv";
            }

            try {
                CSVWriter writer = new CSVWriter(new FileWriter(path), ',', CSVWriter.NO_QUOTE_CHARACTER);

//				Schreiben der einzelnen Zeilen der CSV Datei
                for (String[] row : coordList) {
                    writer.writeNext(row);
                }

                writer.close();
                System.out.println(path);
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Saves a <tt>Properties</tt> object into a configuration file using a graphical file chooser
     *
     * @param properties <tt>Properties</tt> object that shall be saved.
     * @param home Default folder of the file chooser.
     */
    public static void saveProperties(Properties properties, String home) {
        //		Erstellen der zu speichernden Datei
        JFileChooser chooser = new JFileChooser(home);

        FileNameExtensionFilter filter = new FileNameExtensionFilter("Configuration files '.conf'", "conf");
        chooser.setFileFilter(filter);
        int returnVal = chooser.showSaveDialog(null);


        if(returnVal == JFileChooser.APPROVE_OPTION) {
            String path = chooser.getSelectedFile().getAbsolutePath();
            String extension = path.substring(path.lastIndexOf(".") + 1, path.length());

            if (!extension.equals("conf")) {
                path = path + ".conf";
            }

            try {
                FileWriter writer = new FileWriter(path);

                properties.store(writer, "Configuration file for MipRAn");

                writer.close();
                System.out.println(path);
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
