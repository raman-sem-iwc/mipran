import ij.ImagePlus;
import ij.ImageStack;
import ij.gui.Overlay;
import ij.gui.Roi;
import ij.plugin.MontageMaker;
import ij.plugin.filter.EDM;
import ij.plugin.filter.ParticleAnalyzer;
import ij.process.ByteProcessor;
import ij.process.ImageConverter;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * The real deal.
 * The class does the actual particle finding work.
 *
 * With the instance variable <tt>Measurements</tt> of the properties of the found particles which shall be
 * displayed in the <tt>ResultsTable</tt> can be selected.
 *
 * @author Leonhard Prechtl
 */
public class ParticleFinder {
    private int minPixel = 0;
    private int maxPixel = 1000000000;
    private double minSize = 0;
    private double maxSize = 1000000;
    private float resolution = 1;
    private boolean watershed = false;
    private boolean particlesAreWhite = true;
    private ResultsTableExtra rt = new ResultsTableExtra();

    private ImagePlus overlayImage;
//    public ImagePlus resultImage;
//    private int resultImageWidth = -1;
//    private int resultImageHeight = -1;



    private int rtOptions =
            0 +
				ij.plugin.filter.ParticleAnalyzer.DISPLAY_SUMMARY +
//				ij.plugin.filter.ParticleAnalyzer.SHOW_RESULTS +
//				ij.plugin.filter.ParticleAnalyzer.SHOW_MASKS +
//            ij.plugin.filter.ParticleAnalyzer.SHOW_OVERLAY_MASKS
				ij.plugin.filter.ParticleAnalyzer.SHOW_OVERLAY_OUTLINES
//				ij.plugin.filter.ParticleAnalyzer.INCLUDE_HOLES
//				ij.plugin.filter.ParticleAnalyzer.SHOW_OUTLINES
            ;

//
    private int measurements =
            ij.measure.Measurements.AREA +
                    ij.measure.Measurements.CENTROID +
                    ij.measure.Measurements.FERET +
                    ij.measure.Measurements.SHAPE_DESCRIPTORS
            ;

    /**
     * Basic constructor
     */
    ParticleFinder() {
    }

    /**
     * Constructor which takes all the variables needed parameters for the analysis.
     *
     * @param minPixel Minimum number of coherent pixels in a particle
     *                     Used to rule out noise.
     * @param minSize Minimum length the maximum feret diameter of a particle has to have.
     *                Used to limit the particle sizes for the consecutive measurement.
     * @param maxSize Maximum length the maximum feret diameter of a particle has to have.
     *                Used to limit the particle sizes for the consecutive measurement.
     * @param particlesAreWhite <tt>true</tt> Light particles on dark background.
     *                          For dark-field microscopy images.
     *                          <tt>false</tt> Dark particles on light background.
     *                          For bright-field microscopy images.
     *
     * @param resolution Resolution of the microscopy image in pixel/µm.
     * @param watershed <tt>true</tt> Separate the particles with the help of the watershed algorithm.
     */
    ParticleFinder(int minPixel,
                   double minSize,
                   double maxSize,
                   float resolution,
                   boolean particlesAreWhite,
                   boolean watershed) {
        this.minPixel = minPixel;
        this.minSize = minSize;
        this.maxSize = maxSize;
        this.resolution = resolution;
        this.particlesAreWhite = particlesAreWhite;
        this.watershed = watershed;
    }

    /**
     * The core method of the software.
     * This method finds the particles.
     *
     * @param imp Image on which the particle finder algorithm shall be applied.
     */
    public void analyze20(ImagePlus imp) {

        ImagePlus imporg = (ImagePlus) imp.clone();

//		convert image into grayscale
        ImageConverter conv = new ImageConverter(imp);
        conv.convertToHSB();

//		get value image out of HSV channel stack
        ImagePlus impV = new ImagePlus("Value", imp.getStack().getProcessor(3));

//		autothreshold image
        impV.getProcessor().setAutoThreshold(ij.process.AutoThresholder.Method.Otsu, particlesAreWhite);



//		if the image should be watersheded the calculations are done in this if clause
//		TODO: create a new method for watershedding, which takes the thresholded image and returns the watersheded thresholded image
        if (watershed) {
//			Find particles in Thresholded image
//			Neccesary creates Particle mask as overlay
            int rtOptions =
//					ij.plugin.filter.ParticleAnalyzer.DISPLAY_SUMMARY +
//					ij.plugin.filter.ParticleAnalyzer.SHOW_RESULTS +
//					ij.plugin.filter.ParticleAnalyzer.SHOW_MASKS +
                    ij.plugin.filter.ParticleAnalyzer.SHOW_OVERLAY_MASKS
//					ij.plugin.filter.ParticleAnalyzer.SHOW_OVERLAY_OUTLINES
//					ij.plugin.filter.ParticleAnalyzer.INCLUDE_HOLES
//					ij.plugin.filter.ParticleAnalyzer.SHOW_OUTLINES
                    ;

            ResultsTableExtra rtWat = new ResultsTableExtra();
            ParticleAnalyzer analyWat = new ParticleAnalyzer(rtOptions, 0, rtWat, minPixel, maxPixel);
            analyWat.analyze(impV);

//			Create new image out of overlay
            ImagePlus impThres = new ImagePlus("Threshold image", new ByteProcessor(impV.getProcessor().getWidth(), impV.getProcessor().getHeight()));
            impThres.setOverlay(impV.getOverlay());
            if (impThres.getOverlay() != null) {
                impThres.getOverlay().setFillColor(java.awt.Color.WHITE);
                impThres.getOverlay().drawLabels(false);
            }
            impThres = impThres.flatten();
            impThres.setProcessor(impThres.getProcessor().convertToByteProcessor());

//			watershed thresholded image
            EDM distance = new EDM();
            distance.toWatershed(impThres.getProcessor());

//			create a threshold overlay, as image is binary (0 and 255) the exact lower threshold value (50) does not matter and the upper threshold value has to bee 255.
            impThres.getProcessor().setThreshold(50, 255, ij.process.ImageProcessor.RED_LUT);



            impV = (ImagePlus) impThres.clone();
        }

        rt = new ResultsTableExtra();

        ParticleAnalyzer analy = new ParticleAnalyzer(rtOptions, measurements, rt, minPixel, maxPixel);


//		find particles in thresholded image
        analy.analyze(impV);

        rt.setConstantRowNumber();
//        System.out.println(rt.size());
        try {
            rt.convertToUm("Feret", resolution, 1);
            rt.convertToUm("MinFeret", resolution, 1);
            rt.convertToUm("Area", resolution, 2);
            rt.deleteRowsExtrema("umFeret", minSize, maxSize);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }



//		Remove Threshold drawing out of Image
        impV.getProcessor().resetThreshold();


//		Draw particle outlines green and thicker
        Overlay overlay = impV.getOverlay();

//		skip if no particles found
        if (overlay != null ) {
            overlay.setStrokeColor(java.awt.Color.GREEN);
            int num = 0;
            for (int i=0; i<overlay.size(); i++) {
                Roi roi = overlay.get(i);
                if (roi.getFeretValues()[0]<=maxSize && roi.getFeretValues()[0]>=minSize) {
                    roi.setStrokeWidth(2.0);
                    num++;
                }
                else {
                    roi.setStrokeColor(java.awt.Color.RED);
                    roi.setStrokeWidth(2.0);
                }

            }
//            System.out.println(num);
        }
        else {
            System.out.println("no particles found");
        }

        imporg.setOverlay(overlay);


        overlayImage = imporg;
    }

    /**
     * Opens all images in the folder (ignores subfolders) and stitches them to one big image.
     * The positions of the images in the stitched images are determined out of their filenames.
     *
     * The upper left image must have the name:
     *      abc-000-000xyz
     *      abc and xyz lenghs can be choosen freely, xyz may not contain '-'.
     * The second image in the top row must have the name:
     *      abc-001-000xyz
     * The second image in the left column must have the name:
     *      abc-000-001xyz
     *
     * This naming convention is honored by the ImageStitching software written in Labview.
     *
     * @param folder The folder which contains the image files.
     * @return the stitched image
     */
    public static ImagePlus stitchImages(File folder) {
        int columns;
        int rows;

        ArrayList<ImagePlusPath> impArray = Files.folderImageOpener(folder, false);

        Collections.sort(impArray, new Comparator<ImagePlusPath>() {
            @Override
            public int compare(ImagePlusPath impP1, ImagePlusPath impP2)
            {
                String path1 = impP1.getPath().toString();
                String path2 = impP2.getPath().toString();

                int rowIndex1 = path1.lastIndexOf('-');
                int rowIndex2 = path2.lastIndexOf('-');

                int columnIndex1 = path1.lastIndexOf('-', rowIndex1 - 1);
                int columnIndex2 = path2.lastIndexOf('-', rowIndex2 - 1);

                String numStr1 = path1.substring(rowIndex1 + 1, rowIndex1 + 4) + path1.substring(columnIndex1 + 1, columnIndex1 + 4);
                String numStr2 = path2.substring(rowIndex2 + 1, rowIndex2 + 4) + path2.substring(columnIndex2 + 1, columnIndex2 + 4);

//				 	int num1 = Integer.parseInt(numStr1);
//				 	int num2 = Integer.parseInt(numStr2);


                return numStr1.compareTo(numStr2);
            }
        });

        String lastPath = impArray.get(impArray.size() - 1).getPath().toString();
        int rowIndex = lastPath.lastIndexOf('-');
        int columnIndex = lastPath.lastIndexOf('-', rowIndex - 1);
        columns = Integer.parseInt(lastPath.substring(columnIndex + 1, columnIndex + 4)) + 1;
        rows = Integer.parseInt(lastPath.substring(rowIndex + 1, rowIndex + 4)) + 1;


        ImageStack stack = new ImageStack(impArray.get(1).getImagePlus().getWidth(), impArray.get(1).getImagePlus().getHeight());
        for (ImagePlusPath imp : impArray) {
            stack.addSlice(imp.getImagePlus().getProcessor());
        }

        ImagePlus impStack = new ImagePlus("Stack", stack);

        MontageMaker montage = new MontageMaker();
        ImagePlus stitched = montage.makeMontage2(impStack, columns, rows, 1, 1, stack.getHeight(), 1, 0, false);

        return stitched;
    }

    /**
     * Converts the X and the Y coordinates into the µm scale.
     * Saves the calculated coordinates directly in the <tt>ResultsTable</tt>.
     * Uses the resolution which is stored in the instance variable
     *
     * @param baseX X offset of the image center in µm
     * @param baseY Y offset of the image center in µm
     */
    public void calculateCoordinates(double baseX, double baseY){
        rt.calcCoord(overlayImage.getProcessor().getWidth(), overlayImage.getProcessor().getHeight(), resolution, baseX, baseY);
    }

    public List<String[]> exportCoordinates() {
        return rt.coordExport();
    }

    public List<String[]> saveRt() {
        return rt.saveRt("Number", "umX", "umY", "umFeret", "umMinFeret", "umArea");
    }

//    public List<String[]> exportResults(double baseX, double baseY) {
//   TODO
//    }

    public ResultsTableExtra getRt() {
        return rt;
    }

    /**
     * Sets the minPixel instance variable.
     * Checks wether the new minPixal value is {@code >= 0}
     *
     * @param minPixel New value for this.minPixel.
     *                 Has to be {@code >= 0}
     * @throws IllegalArgumentException When {@code minPixel < 0}
     */
    public void setMinPixel(int minPixel)
                throws IllegalArgumentException {
        if (minPixel >= 0) {
            this.minPixel = minPixel;
        } else {
            throw new IllegalArgumentException();
        }
    }

    public void setMaxPixel(int maxPixel) {
        this.maxPixel = maxPixel;
    }


    public void setRtOptions(int rtOptions) {
        this.rtOptions = rtOptions;
    }


    public void setWatershed(boolean watershed) {
        this.watershed = watershed;
    }

    public void setParticlesAreWhite(boolean particlesAreWhite) {
        this.particlesAreWhite = particlesAreWhite;
    }

    public ImagePlus getOverlayImage() {
        return overlayImage;
    }
}