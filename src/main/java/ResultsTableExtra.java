import ij.measure.ResultsTable;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.util.ArrayList;
import java.util.List;

/**
 * Extention of the <tt>ResultsTable</tt> class of ImageJ with own methods for result handling and export.
 *
 * @author Leonhard Prechtl
 */
public class ResultsTableExtra extends ResultsTable {


    /**
     * Creates a new column in Resultstable which uses µm as scale instead of pixels.
     * The new column name is "um" + columnName
     *
     * @param columnName Name of the column to be converted
     * @param resolution in pixel/µm
     * @param power To which power of the resolution the column scales
     *              Mostly 1, for areas e.g. 2
     * @throws IllegalArgumentException When the column columnName does not exist in the <tt>ResultstableExtra</tt>
     */

	public void convertToUm (String columnName, float resolution, int power)
                throws IllegalArgumentException {
//		Return false if column 'columnName' does not exist
		if (!this.columnExists(this.getColumnIndex(columnName))) {
			throw new IllegalArgumentException("Column " + columnName + " does not exist in Resutlstable");
		}
		
		double[] originalColumn = this.getColumnAsDoubles(this.getColumnIndex(columnName));
		double[] umColumn = new double[originalColumn.length];
		
		for (int i = 0; i < originalColumn.length; i++) {
			umColumn[i] = originalColumn[i] / Math.pow(resolution, power);
		}
		
		for (int i = 0; i < umColumn.length; i++) {
			this.setValue("um" + columnName, i, umColumn[i]);
		}
	}

	public void setConstantRowNumber(){
	    int length = this.size();
//	    System.out.println(length);
        for (int i = 0; i < length; i++){
            this.setValue("Number", i, i+1);
//            System.out.println(i + "\t" + this.getValueAsDouble(this.getColumnIndex("Number"), i));
        }
    }

    /**
     * Deletes all rows in <tt>ResultsTableExtra</tt> where values in column "columnName"
     * are not between <tt>"minValue"</tt> and <tt>"maxValue"</tt>
     *
     * @param columnName Name of the column.
     * @param minValue Minimum value still allowed in the column.
     * @param maxValue Maximum value still allowed in the column.
     * @throws IllegalArgumentException When the column columnName does not exist in the <tt>ResultstableExtra</tt>
     */
	public void deleteRowsExtrema (String columnName, double minValue, double maxValue)
		throws IllegalArgumentException
	{
		if (!this.columnExists(this.getColumnIndex(columnName))) {
			throw new IllegalArgumentException("Column " + columnName + " does not exist in Resutlstable");
		}
		
		double[] columnArray = this.getColumnAsDoubles(this.getColumnIndex(columnName));
		
		//		running form highest row index to 0 because rows indices change if rows below are deleted
		for (int i = columnArray.length -1; i >= 0; i--) {
			if (minValue > columnArray[i] || maxValue < columnArray[i]) {
				this.deleteRow(i);
			}
		}
	}

    /**
     * Saves the <tt>ResultsTableExtra</tt> into a csv file with the help of a graphical file chooser.
     *
     * @param home Default folder of the file chooser.
     */
	public void rtSaver(String home) {
		JFileChooser chooser = new JFileChooser(home);

		FileNameExtensionFilter filter = new FileNameExtensionFilter("CSV files", "csv");
		chooser.setFileFilter(filter);
		int returnVal = chooser.showSaveDialog(null);
	
		
		if(returnVal == JFileChooser.APPROVE_OPTION) {
			String path = chooser.getSelectedFile().getAbsolutePath();
			String extension = path.substring(path.lastIndexOf(".") + 1, path.length());
			
			if (!extension.equals("csv")) {
				path = path + ".csv";
			}
			
		    try {
		        this.save(path);
		        	
//		    FileWriter fw = new FileWriter(chooser.getSelectedFile()+".txt");
//		    fw.write(rt.toString());
		     } catch (Exception e) {
		            e.printStackTrace();
		     }
		}
	}

    /**
     * Converts the X and Y column into coordinates in µm scale.
     * Saves the calculated coordinates in column named 'umX' and 'umY'.
     *
     * In the new coordinates the image center has the coordinates 'baseX', 'baseY'.
     *
     * @param width Width of the image.
     * @param height Height of the image.
     * @param resolution in pixel/µm.
     * @param baseX X offset of the image center in µm (as in WiTEC Control).
     * @param baseY X offset of the image center in µm (as in WiTEC Control).
     */
	public void calcCoord(int width, int height, float resolution, double baseX, double baseY) {
        double[] X = this.getColumnAsDoubles(this.getColumnIndex("X"));
        double[] Y = this.getColumnAsDoubles(this.getColumnIndex("Y"));

        for (int i = 0; i < X.length; i++) {
//			Umrechnen der Koordinaten in µm
            X[i] = X[i] / resolution;

//			Verschieben von 0|0 in den Bildmittelpunkt
            X[i] -= width / (2 * resolution);

//			Addieren einer eventuellen Verschiebung des Bildmittelpunktes in der Witec Control Software
            X[i] += baseX;

//          Einfügen der µm Koordinaten in die Resultstable
            this.setValue("umX", i, X[i]);
        }
        for (int i = 0; i < Y.length; i++) {
//			Umrechnen der Koordinaten in µm
            Y[i] = -Y[i] / resolution;

//			Verschieben von 0|0 in den Bildmittelpunkt
            Y[i] += height / (2 * resolution);

//			Addieren einer eventuellen Verschiebung des Bildmittelpunktes in der Witec Control Software
            Y[i] += baseY;

            //Einfügen der µm Koordinaten in die Resultstable
            this.setValue("umY", i, Y[i]);
        }


    }

//	public List<String[]> calcCoord(int size, float resolution, double baseX, double baseY, int firstnumber) {
//        float[] xcoord = this.getColumn(this.getColumnIndex("X"));
//        float[] ycoord = this.getColumn(this.getColumnIndex("Y"));
//        for (int i = 0; i < xcoord.length; i++) {
////			Umrechnen der Koordinaten in µm
//            xcoord[i] = xcoord[i] / resolution;
//
////			Verschieben von 0|0 in den Bildmittelpunkt
//            xcoord[i] -= size / (2 * resolution);
//
////			Addieren einer eventuellen Verschiebung des Bildmittelpunktes in der Witec Control Software
//            xcoord[i] += baseX;
//        }
//        for (int i = 0; i < ycoord.length; i++) {
////			Umrechnen der Koordinaten in µm
//            ycoord[i] = -ycoord[i] / resolution;
//
////			Verschieben von 0|0 in den Bildmittelpunkt
//            ycoord[i] += size / (2 * resolution);
//
////			Addieren einer eventuellen Verschiebung des Bildmittelpunktes in der Witec Control Software
//            ycoord[i] += baseY;
//        }
//
//
//        List<String[]> coordList = new ArrayList<>(0);
//
//        //Erstellen der einzelnen Zeilen der CSV Datei
//
//        for (int i = 0; i < xcoord.length; i++) {
//            String[] row = new String[3];
////					Spalte 1: Indexnummer des Partikels
//            row[0] = String.valueOf(i + firstnumber);
//
////					Spalte 2: x Koordinate des Partikels
//            row[1] = String.valueOf(xcoord[i]);
//
////					Spalte 3: y Kooordinate des Partikels
//            row[2] = String.valueOf(ycoord[i]);
//
//            coordList.add(row);
//        }
//
//        return coordList;
//    }

    /**
     * Generates a <tt>List</tt> of <tt>String[]</tt> of the 'umX' and 'umY' columns for coordinate Export.
     * E.g. with <tt>Files.saveCoordListCSV()</tt>
     *
     * @return List of coordinates.
     *         Each List element should contain one particle
     *         String[0] its number (Starting with 1)
     *         String[1] X coordinate
     *         String[2] Y coordinate
     * @throws IllegalStateException 'umX' or 'umY' do not exist. Calculate them with <tt>calcCoord()</tt>
     */
	public List<String[]> coordExport()
           throws IllegalStateException {
//	    int firstnumber = 1;

        if (!this.columnExists(this.getColumnIndex("Number"))) {
            throw new IllegalStateException("No row numbers calculated yet! Use setConstantRowNumber()");
        }
        if (!this.columnExists(this.getColumnIndex("umX"))) {
            throw new IllegalStateException("No Coordinates calculated yet! Use calcCoord()");
        }
        if (!this.columnExists(this.getColumnIndex("umY"))) {
            throw new IllegalStateException("No Coordinates calculated yet! Use calcCoord()");
        }

        double[] numbers = this.getColumnAsDoubles(this.getColumnIndex("Number"));
        double[] X = this.getColumnAsDoubles(this.getColumnIndex("umX"));
        double[] Y = this.getColumnAsDoubles(this.getColumnIndex("umY"));

        List<String[]> coordList = new ArrayList<>(0);

        //Erstellen der einzelnen Zeilen der Liste

        for (int i = 0; i < X.length; i++) {
            String[] row = new String[3];
//					Spalte 1: Indexnummer des Partikels
            row[0] = String.valueOf((int) Math.round(numbers[i]));

//					Spalte 2: x Koordinate des Partikels
            row[1] = String.valueOf(X[i]);

//					Spalte 3: y Kooordinate des Partikels
            row[2] = String.valueOf(Y[i]);

            coordList.add(row);
        }

        return coordList;
	}

    /**
     * Generates a <tt>List</tt> of <tt>String[]</tt> of the given columns contents.
     *
     * @param columns Names of the columns that shall be exported
     * @return List of values.
     *         Each List element should contain one particle
     *         String[0] its number (Starting with 1)
     *         String[n] value of the respective column
     * @throws IllegalArgumentException A column in columns does not exist.
     */
    public List<String[]> saveRt(String... columns)
           throws IllegalArgumentException {
//        int firstnumber = 1;

        for (String c: columns) {
            if (!this.columnExists(this.getColumnIndex(c))) {
                throw new IllegalArgumentException("Column " + c + " does not exist in Resutlstable");
            }
        }

        List<String[]> table = new ArrayList<>(columns.length);

        int len = this.getColumnAsDoubles(this.getColumnIndex("X")).length;

        String[] header = new String[columns.length+1];
        header[0] = "#";
        for (int j = 0; j < columns.length; j++) {
            header[j+1] = String.valueOf(columns[j]);
        }
        table.add(header);

        for (int i = 0; i < len; i++) {
            String[] row = new String[columns.length+1];
////					Spalte 1: Indexnummer des Partikels
//            row[0] = String.valueOf(i + firstnumber);

            for (int j = 0; j < columns.length; j++) {
//                row[j+1] = String.valueOf(this.getValue(columns[j], i));
                row[j] = String.valueOf(this.getValue(columns[j], i));
            }

            table.add(row);
        }

        return table;
    }
}