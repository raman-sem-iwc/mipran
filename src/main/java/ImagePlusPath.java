import ij.ImagePlus;

import java.io.File;

/**
 * 
 * The ImagePlusPath class combines an ImagePlus Object with a File Object.
 * 
 * Reason: enable to both Objects to be returned together.
 * 
 * @author Leonhard Prechtl
 */

public class ImagePlusPath {
	
	private File path;
	private ImagePlus imagep;

	public ImagePlusPath(ImagePlus img, File file) {
		imagep = img;
		path = file;
	}

	public ImagePlus getImagePlus () {
		return imagep;
	}
	
	public File getPath() {
		return path;
	}
	
	public void setImagePlus (ImagePlus img) {
		imagep = img;
	}
	
	public void setPath(File newPath) {
		path = newPath;
	}
}
