import ij.ImagePlus;
import ij.io.FileSaver;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Working class for the command line version of MipRAn.
 *
 * Stores settings.
 * Calls the particle finder algorithms.
 *
 * <p><b>CURRENTLY NOT USABLE! BROKEN!</b></p>
 *
 * @author Leonhard Prechtl
 */
public class Runner_cli {

    //		parameter initialization and default values
    private int particleSize = 100;					//minimum pixels per particle
    private double minSize = 0;				//minimum particle size in µm
    private double maxSize = 1000000;				//maximum particle size in µm
    private boolean particlesAreWhite = true;		//false: dark particles on light background; true: light particles on dark background
    private String protocol = "20";					//if different methods are used for different images, the selection is saved by this parameter; e.g. objective resolution or fluoreszenz im
    private float resolution = 1;					//resolution in pixels per µm in microscopy image
    private boolean coordExport = false;			//export coordinates of the found particles into a file (for Witek control import)
    private boolean saveRt = false;					//save found particles and their area into file
    private boolean saveMask = false;				//save found particles mask into file
    private double coordinateBaseX = 0;				//x coordinate in µm of the image center (for coordinate export)
    private double coordinateBaseY = 0;				//y coordinate in µm of the image center (for coordinate export)
    private boolean watershed = false;				//Watershed found particles

    String home = "/home/leonhard/Studium/17S/FP_Mikroplasik/Pattern/witek";
//    String home = "/run/media/leonhard";

    public Runner_cli() {

    }

//  TODO: change to complete cli control without usage of graphical file chooser
//  TODO: create a GUI

    public void run() {
        if (protocol.equals("20")) {
            ImagePlus imp = Files.imageOpener(this.home);

            ParticleFinder finder = new ParticleFinder();
            finder.setMinPixel(20);
            finder.setWatershed(this.watershed);
            finder.setParticlesAreWhite(this.particlesAreWhite);
            finder.analyze20(imp);
            ImagePlus maskImage = finder.getOverlayImage();
            ResultsTableExtra rt = finder.getRt();

            maskImage = maskImage.flatten();
            if (this.saveMask) {
                try {
                    FileSaver save = new FileSaver(maskImage);
                    save.saveAsPng();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }



//            export coordinates as .csv for witek control software
//            coordinateBase: coordinates of the image center in the witek control software
            if (this.coordExport) {
                rt.coordExport();
            }

        } else if (protocol.equals("20folder")) {
            File folderpath = Files.folderChooser(this.home);
            ArrayList<ImagePlusPath> impArray = Files.folderImageOpener(folderpath, false);
            List<String[]> infos = Files.folderInfoOpener(folderpath);
            List<String[]> allCoordList = new ArrayList<>();



            for (ImagePlusPath imp : impArray) {
                ParticleFinder finder = new ParticleFinder();
                finder.setMinPixel(20);
                finder.setWatershed(this.watershed);
                finder.setParticlesAreWhite(this.particlesAreWhite);
                finder.analyze20(imp.getImagePlus());
                ImagePlus maskImage = finder.getOverlayImage();
                ResultsTableExtra rt = finder.getRt();

                String[] impInfo = Methods.searchList(infos, 0, imp.getPath().getName());



//                String name = imp.getPath().getName();
//                int lastDot = name.lastIndexOf('.');
//                String path = imp.getPath().getParent() + File.separator + name.substring(0, lastDot) + "_Overlay" + name.substring(lastDot);

//                System.out.println(path);


                maskImage = maskImage.flatten();
                if (this.saveMask) {
                    String savePath = imp.getPath().getParent() + File.separator + "mask_" + imp.getPath().getName();
                    System.out.println(savePath);
                    try {
                        FileSaver save = new FileSaver(maskImage);
                        save.saveAsPng(savePath);
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                }

                if (this.coordExport) {
                    double baseX = this.coordinateBaseX + Double.parseDouble(impInfo[1]);
                    double baseY = this.coordinateBaseY + Double.parseDouble(impInfo[2]);
                    List<String[]> coordList = rt.coordExport();
                    for (int i = 0; i < coordList.size(); i++) {
                        String[] row = coordList.get(i);
                        row[0] = imp.getPath().getName() + "_" + row[0];
                        coordList.set(i, row);
                    }
                    allCoordList.addAll(coordList);

                }
            }
            if (this.coordExport) {
                Files.saveCoordListCSV(allCoordList, home);
            }
        }
    }

    public int getParticleSize() {
        return particleSize;
    }

    public void setParticleSize(int particleSize) {
        this.particleSize = particleSize;
    }

    public double getMinSize() {
        return minSize;
    }

    public void setMinSize(double minSize) {
        this.minSize = minSize;
    }

    public double getMaxSize() {
        return maxSize;
    }

    public void setMaxSize(double maxSize) {
        this.maxSize = maxSize;
    }

    public boolean isParticlesAreWhite() {
        return particlesAreWhite;
    }

    public void setParticlesAreWhite(boolean particlesAreWhite) {
        this.particlesAreWhite = particlesAreWhite;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public float getResolution() {
        return resolution;
    }

    public void setResolution(float resolution) {
        if (resolution > 0) {
            this.resolution = resolution;
        }
        else {
            throw new IllegalArgumentException();
        }
    }

    public boolean isCoordExport() {
        return coordExport;
    }

    public void setCoordExport(boolean coordExport) {
        this.coordExport = coordExport;
    }

    public boolean isSaveRt() {
        return saveRt;
    }

    public void setSaveRt(boolean saveRt) {
        this.saveRt = saveRt;
    }

    public boolean isSaveMask() {
        return saveMask;
    }

    public void setSaveMask(boolean saveMask) {
        this.saveMask = saveMask;
    }

    public double getCoordinateBaseX() {
        return coordinateBaseX;
    }

    public void setCoordinateBaseX(double coordinateBaseX) {
        this.coordinateBaseX = coordinateBaseX;
    }

    public double getCoordinateBaseY() {
        return coordinateBaseY;
    }

    public void setCoordinateBaseY(double coordinateBaseY) {
        this.coordinateBaseY = coordinateBaseY;
    }

    public boolean isWatershed() {
        return watershed;
    }

    public void setWatershed(boolean watershed) {
        this.watershed = watershed;
    }






}
