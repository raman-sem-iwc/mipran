

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.Locale;
import java.util.Properties;

/**
 * This class contains the main method for the GUI version of MipRAn.
 * It also creates the GUI. The GUI was designed using the UI-Builder of IntelliJ.
 *
 * @author Leonhard Prechtl
 */
public class MipRAn_gui {
    private JLabel label_origImage;
    private JButton button_openImage;
    private JButton button_analyze;
    private JPanel panel_main;
    private JLabel label_resImage;
    private JFormattedTextField formattedTextField_minpx;
    private JLabel label_minpx;
    private JLabel label_minSize;
    private JLabel label_maxSize;
    private JLabel label_particlesWhite;
    private JLabel label_protocol;
    private JLabel label_resolution;
    private JLabel label_watershed;
    private JLabel label_coordinateBaseX;
    private JLabel label_coordinateBaseY;
    private JFormattedTextField formattedTextField_minSize;
    private JFormattedTextField formattedTextField_maxSize;
    private JFormattedTextField formattedTextField_resolution;
    private JFormattedTextField formattedTextField_coordinateBaseX;
    private JFormattedTextField formattedTextField_coordinateBaseY;
    private JCheckBox checkBox_particlesWhite;
    private JComboBox comboBox_protocol;
    private JCheckBox checkBox_watershed;
    private JButton button_exportCoordinates;
    private JButton button_saveResults;
    private JButton button_saveOverlayImage;
    private JButton quitButton;
    private JButton button_importSettings;
    private JButton button_saveSettings;
    private JLabel label_legend;
    private JLabel label_size;
    private JLabel label_memory;
    private Runner_gui runner;


    /**
     * Constructor of the gui.
     * Initialises the runner and adds <tt>ActionListener</tt> to the buttons in the GUI
     */
    public MipRAn_gui() {
        runner = new Runner_gui();

        button_openImage.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String protocol = "";
                if (comboBox_protocol.getModel().getSelectedItem().equals("Single Image")) {
                    protocol = "single";
                } else if (comboBox_protocol.getModel().getSelectedItem().equals("Stitching Image")){
                    System.out.println("f1");
                    protocol = "stitching";
                }
                if (runner.openImage(protocol)) {
                    BufferedImage img = runner.getOriginalImage().getBufferedImage();
                    ImageIcon icon = new ImageIcon(resizePreviewImage(img));
                    label_origImage.setIcon(icon);
//                    Show images resolution
                    label_size.setText(String.valueOf(img.getWidth() + " × " + String.valueOf(img.getHeight())) + " px");

////                    Expicit garbage collection to remove now unneeded old images from heap.
//                    System.gc();
                }
            }
        });
        button_analyze.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
//                Delete Painting of original and result image to free memory
                Image prevImg = resizePreviewImage(null);
                label_resImage.setIcon(new ImageIcon(prevImg));
                label_origImage.setIcon(new ImageIcon(prevImg));
                label_legend.setText("");
////              Expicit garbage collection to remove now unneeded old images from heap.
//                System.gc();

//                Read parameters and analyze image
                try {
                    formattedTextField_minpx.commitEdit();
                    formattedTextField_minSize.commitEdit();
                    formattedTextField_maxSize.commitEdit();
                    formattedTextField_resolution.commitEdit();
                    formattedTextField_coordinateBaseX.commitEdit();
                    formattedTextField_coordinateBaseY.commitEdit();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                runner.setParticleSize(Integer.parseInt(formattedTextField_minpx.getText()));
                runner.setMinSize(Double.parseDouble(formattedTextField_minSize.getText()));
                runner.setMaxSize(Double.parseDouble(formattedTextField_maxSize.getText()));
                runner.setResolution(Float.parseFloat(formattedTextField_resolution.getText()));
                runner.setParticlesAreWhite(checkBox_particlesWhite.isSelected());
                runner.setWatershed(checkBox_watershed.isSelected());
                runner.setCoordinateBaseX(Double.parseDouble(formattedTextField_coordinateBaseX.getText()));
                runner.setCoordinateBaseY(Double.parseDouble(formattedTextField_coordinateBaseY.getText()));
                runner.setProtocol((String) comboBox_protocol.getModel().getSelectedItem());
                if (comboBox_protocol.getModel().getSelectedItem().equals("Single Image")) {
                    runner.setProtocol("single");
                } else if (comboBox_protocol.getModel().getSelectedItem().equals("Stitching Image")) {
                    runner.setProtocol("stitching");
                } else {
                    System.err.println("Unknown Protokoll: " + (String) comboBox_protocol.getModel().getSelectedItem());
                }

//                Show preview of analyzed image with particle overlays
                runner.analyze();
                BufferedImage img = runner.getBufferedResultImage();
                ImageIcon icon = new ImageIcon(resizePreviewImage(img));
                label_resImage.setIcon(icon);

//                repaint original Image
                BufferedImage imgorg = runner.getOriginalImage().getBufferedImage();
                ImageIcon iconorg = new ImageIcon(resizePreviewImage(imgorg));
                label_origImage.setIcon(iconorg);

//                Create a legend for the images dimensions
                double umwidth = img.getWidth() / runner.getResolution();
                double zoom = icon.getIconWidth() / umwidth;
                double legendRawLength = umwidth/5;
                double magnitude = Math.pow(10, Math.floor(Math.log10(legendRawLength)));
                double legendLength = Math.round(legendRawLength / magnitude) * magnitude;
                int legendWidth = (int) Math.round(legendLength * zoom);
                label_legend.setText(Methods.doubleToFormattedString(legendLength) + " µm");
                try {
                    BufferedImage legendRawImage;
                    legendRawImage = ImageIO.read(getClass().getResource("legend100x15.png"));
                    Image legendImage = legendRawImage.getScaledInstance(legendWidth, 15, Image.SCALE_SMOOTH);
                    label_legend.setIcon(new ImageIcon(legendImage));
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
        });
        quitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                System.exit(0);
            }
        });
        button_exportCoordinates.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                runner.exportCoordinates();
            }
        });
        button_saveOverlayImage.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                runner.saveOverlayImage();
            }
        });
        button_saveResults.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                runner.saveResults();
            }
        });
        button_saveSettings.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    formattedTextField_minpx.commitEdit();
                    formattedTextField_minSize.commitEdit();
                    formattedTextField_maxSize.commitEdit();
                    formattedTextField_resolution.commitEdit();
                    formattedTextField_coordinateBaseX.commitEdit();
                    formattedTextField_coordinateBaseY.commitEdit();
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                runner.saveSettings(
                        formattedTextField_minpx.getValue(),
                        formattedTextField_minSize.getValue(),
                        formattedTextField_maxSize.getValue(),
                        checkBox_particlesWhite.isSelected(),
                        comboBox_protocol.getModel().getSelectedItem(),
                        formattedTextField_resolution.getValue(),
                        formattedTextField_coordinateBaseX.getValue(),
                        formattedTextField_coordinateBaseY.getValue(),
                        checkBox_watershed.isSelected());
            }
        });
        button_importSettings.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Properties loadProp = runner.loadSettings();
                String p;
                if ((p = loadProp.getProperty("particleSize")) != null) {
                    formattedTextField_minpx.setValue(Integer.parseInt(p));
                }
                if ((p = loadProp.getProperty("minSize")) != null) {
                    formattedTextField_minSize.setValue(Double.parseDouble(p));
                }
                if ((p = loadProp.getProperty("maxSize")) != null) {
                    formattedTextField_maxSize.setValue(Double.parseDouble(p));
                }
                if ((p = loadProp.getProperty("black")) != null) {
                    checkBox_particlesWhite.setSelected(!Boolean.parseBoolean(p));
                }
                if ((p = loadProp.getProperty("protocol")) != null) {
                    comboBox_protocol.getModel().setSelectedItem(String.valueOf(p));
                }
                if ((p = loadProp.getProperty("resolution")) != null) {
                    formattedTextField_resolution.setValue(Double.parseDouble(p));
                }
                if ((p = loadProp.getProperty("basex")) != null) {
                    formattedTextField_coordinateBaseX.setValue(Double.parseDouble(p));
                }
                if ((p = loadProp.getProperty("basey")) != null) {
                    formattedTextField_coordinateBaseY.setValue(Double.parseDouble(p));
                }
                if ((p = loadProp.getProperty("watershed")) != null) {
                    checkBox_watershed.setSelected(Boolean.parseBoolean(p));
                }
            }
        });
    }

    /**
     * Resizes the image to fill out about half of the main window width minus the width of the sidebar.
     *
     * @param img The image that shell be resized.
     * @return The resized image.
     *         If img == null returns the resized default image (white).
     */
    private Image resizePreviewImage(BufferedImage img) {
        double factorX = 0.48;
        double summandX = 300;

        if (img == null) {
            try {
//                img = ImageIO.read(new File("src/main/resources/defaultImg.png"));
                img = ImageIO.read(getClass().getResource("defaultImg.png"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        double newWidth = (panel_main.getWidth() - summandX) * factorX;
        double scale = newWidth/img.getWidth();
        double newHeight = img.getHeight()*scale;
//        System.out.println(panel_main.getWidth());

        return img.getScaledInstance((int) Math.round(newWidth), (int) Math.round(newHeight), Image.SCALE_SMOOTH);
    }


    /**
     * Generates a <tt>DecimalFormat</tt> which can be used to format a <tt>JFormattedTextField</tt>
     * so that it only takes inputs which have the desired format.
     *
     * @param type Name of the desired format.
     *             Possible values: "Integer" for Integer numbers e.g. 10
     *                              "Double" for floating point numbers e.g. 10.43
     * @return the generated <tt>DecimalFormat</tt>
     */
    private DecimalFormat createDecimalFormat(String type) {
        DecimalFormatSymbols symb = new DecimalFormatSymbols(new Locale("en", "US"));
        symb.setGroupingSeparator(' ');

        String formatString;
        switch (type) {
            case "Integer": formatString = "###,###";
                break;
            case "Double": formatString = "###,###.###";
                break;
            default: throw new IllegalArgumentException();
        }
        DecimalFormat formatter = new DecimalFormat(formatString, symb);
        formatter.setGroupingUsed(false);
        return formatter;
    }

    /**
     * Starting point of the GUI version of MipRAn
     *
     * Startup and initialisation of the GUI
     *
     * @param args Ignores arguments
     */
    public static void main(String[] args) {
        JFrame frame = new JFrame("MipRAn");
        MipRAn_gui gui = new MipRAn_gui();
        frame.setContentPane(gui.panel_main);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        gui.initializeUIComponents();

        gui.label_memory.setText("");



////      Memory measurement
//        int iter = 0;
//        while(true) {
//            long millis = System.currentTimeMillis();
//            iter++;
//
//            MemoryMeter meter = new MemoryMeter();
//            double framesizemb = meter.measureDeep(frame)/1024/1024;
////            double guisizemb = meter.measureDeep(gui)/1024/1024;
////            System.out.println(Integer.toString(iter) + "\tFrame: " + Double.toString(framesizemb) + " MiB");
//            gui.label_memory.setText("Memory: " + Double.toString(framesizemb) + " MiB");
//
//            try {
//                Thread.sleep(5000 - millis % 5000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }

    }

    /**
     * Custom creation of certain UI components.
     */
    private void createUIComponents() {
        formattedTextField_minpx = new JFormattedTextField(createDecimalFormat("Integer"));
        formattedTextField_minSize = new JFormattedTextField(createDecimalFormat("Double"));
        formattedTextField_maxSize = new JFormattedTextField(createDecimalFormat("Double"));
        formattedTextField_resolution = new JFormattedTextField(createDecimalFormat("Double"));
        formattedTextField_coordinateBaseX = new JFormattedTextField(createDecimalFormat("Double"));
        formattedTextField_coordinateBaseY = new JFormattedTextField(createDecimalFormat("Integer"));
    }

    /**
     * Initialisations of certain UI components which have to be executed after panel_main was set visible.
     *
     * Sets blank default images as placeholders for the original and the result image.
     */
    private void initializeUIComponents() {
            Image prevImg = resizePreviewImage(null);
            label_origImage.setIcon(new ImageIcon(prevImg));
            label_resImage.setIcon(new ImageIcon(prevImg));
            label_size.setText("");
            label_legend.setText("");
    }
}
