import ij.ImagePlus;
import java.awt.image.BufferedImage;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.io.File;

import ij.io.FileSaver;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;


/**
 * <p>Working class for the GUI version of MipRAn.</p>
 *
 * <p>Stores settings, handles filechoosers, calls the particle finder algorithms.</p>
 *
 * @author Leonhard Prechtl
 */
public class Runner_gui {
    //		parameter initialization and default values
    private int particleSize = 100;					//minimum pixels per particle
    private double minSize = 0;				//minimum particle size in µm
    private double maxSize = 10000000;				//maximum particle size in µm
    private boolean particlesAreWhite = true;		//false: dark particles on light background; true: light particles on dark background
    private String protocol = "20";					//if different methods are used for different images, the selection is saved by this parameter; e.g. objective resolution or fluoreszenz im
    private float resolution = 1;					//resolution in pixels per µm in microscopy image
    private double coordinateBaseX = 0;				//x coordinate in µm of the image center (for coordinate export)
    private double coordinateBaseY = 0;				//y coordinate in µm of the image center (for coordinate export)
    private boolean watershed = false;				//Watershed found particles

    private ImagePlus originalImage = null;
    private ImagePlus resultImage = null;

    private ParticleFinder finder = new ParticleFinder();

        String home = "/home/leonhard/Studium/17S/FP_Mikroplasik/Pattern/witek";
//    String home = "/run/media/leonhard";

    /**
     * Basic constructor
     */
    Runner_gui(){
    }

    /**
     * Imports an image which is chose with the help of a graphical file chooser.
     * Either a single image or a complete folder which contents get merged into one big image,
     * depending on the chosen protocol.
     *
     * @param protocol The protocol which shall be executed.
     *                 "single" for opening a single image
     *                 "stitching" for merging the contents of a folder.
     * @return <tt>true</tt> if image import was successful
 *             <tt>false</tt> if not (e.g. file opening dialogue was canceled)
     */
    public boolean openImage(String protocol) {
        ImagePlus img = null;
        switch (protocol) {
            case "stitching":
                File folder = Files.folderChooser(home);
                if (folder != null) {
                    img = ParticleFinder.stitchImages(folder);
                }
                break;
            default:
                img = Files.imageOpener(home);
                break;
        }

        if (img == null) {
            return false;
        }
        else {
            originalImage = img;
            return true;
        }
    }

    /**
     * Starts the analysis of the opened image (<tt>openImage()</tt>.
     * Does nothing if no image has been opened.
     */
    public void analyze(){
        finder = new ParticleFinder(particleSize, minSize, maxSize, resolution, particlesAreWhite, watershed);
        if(originalImage != null){
            resultImage = null;
            ImagePlus imp = (ImagePlus) originalImage.clone();
            finder.analyze20(imp);
            finder.calculateCoordinates(coordinateBaseX, coordinateBaseY);
            resultImage = finder.getOverlayImage();
//            originalImage.show();
//            System.out.println(ObjectSizeFetcher.getObjectSize(resultImage));
        }

    }

    /**
     * Saves the resulting overlay image of <tt>analyze()</tt> with the help of a graphical file chooser.
     * Does nothing if no image has been analyzed.
     */
    public void saveOverlayImage(){
        if(resultImage != null) {
            ImagePlus maskImage = resultImage.flatten();
            try {
                FileSaver save = new FileSaver(maskImage);
                save.saveAsPng();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Saves the results on the disk with the help of a graphical file chooser
     */
    public void saveResults(){
        Files.saveCoordListCSV(finder.saveRt(), home);
    }

    /**
     * Exports the coordinates on the disk with the help of a graphical file chooser
     */
    public void exportCoordinates() {
        Files.saveCoordListCSV(finder.exportCoordinates(), home);
    }

    /**
     * Saves the given settings into a config file.
     * Uses a graphical file chooser.
     *
     * @param particleSize Minimum number of coherent pixels in a particle
     *                     Used to rule out noise.
     * @param minSize Minimum length the maximum feret diameter of a particle has to have.
     *                Used to limit the particle sizes for the consecutive measurement.
     * @param maxSize Maximum length the maximum feret diameter of a particle has to have.
     *                Used to limit the particle sizes for the consecutive measurement.
     * @param particlesAreWhite <tt>true</tt> Light particles on dark background.
     *                          For dark-field microscopy images.
     *                          <tt>false</tt> Dark particles on light background.
     *                          For bright-field microscopy images.
     * @param protocol Used analyzation protocol
     *                 <tt>"single"</tt> for single image
     *                 <tt>"stitching"</tt> for multiple images which should be stitched together to
     *                 one big image.
     * @param resolution Resolution of the microscopy image in pixel/µm.
     * @param coordinateBaseX X coordinate of the image center in the µm coordinate system.
     * @param coordinateBaseY Y coordinate of the image center in the µm coordinate system.
     * @param watershed <tt>true</tt> Separate the particles with the help of the watershed algorithm.
     */
    public void saveSettings(Object particleSize, Object minSize, Object maxSize, boolean particlesAreWhite,
                             Object protocol, Object resolution, Object coordinateBaseX, Object coordinateBaseY,
                             boolean watershed) {
        Properties saveProp = new Properties();
        saveProp.setProperty("particleSize", String.valueOf(particleSize));
        saveProp.setProperty("minSize", String.valueOf(minSize));
        saveProp.setProperty("maxSize", String.valueOf(maxSize));
        saveProp.setProperty("black", String.valueOf(!particlesAreWhite));
        saveProp.setProperty("protocol", String.valueOf(protocol));
        saveProp.setProperty("resolution", String.valueOf(resolution));
        saveProp.setProperty("basex", String.valueOf(coordinateBaseX));
        saveProp.setProperty("basey", String.valueOf(coordinateBaseY));
        saveProp.setProperty("watershed", String.valueOf(watershed));

        Files.saveProperties(saveProp, home);
    }

    /**
     * Reads config file from disk.
     * File gets chosen by a graphical file chooser.
     * Converts the config file into a <tt>Properties</tt> object.
     *
     * @return generated properties object
     */
    public Properties loadSettings() {
        Properties loadProp = new Properties();

        JFileChooser chooser = new JFileChooser(home);
        chooser.setFileFilter(new FileNameExtensionFilter("Configuration Files '.conf'", "conf"));
        int returnVal = chooser.showOpenDialog(null);

        if(returnVal == JFileChooser.APPROVE_OPTION) {
            try {
                FileReader reader = new FileReader(chooser.getSelectedFile().getAbsoluteFile());
                loadProp.load(reader);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return loadProp;
    }

    /**
     * Converts the opened image to <tt>BufferedImage</tt> and returns it.
     *
     * @return converted image
     */
    public BufferedImage getBufferedOriginalImage() {
        return originalImage.getBufferedImage();
    }

    /**
     * Converts the result image to <tt>BufferedImage</tt> and returns it.
     *
     * @return converted image
     */
    public BufferedImage getBufferedResultImage() {
        ImagePlus impflat = resultImage.flatten();
        return impflat.getBufferedImage();
    }

    public int getParticleSize() {
        return particleSize;
    }

    public void setParticleSize(int particleSize) {
        this.particleSize = particleSize;
    }

    public double getMinSize() {
        return minSize;
    }

    public void setMinSize(double minSize) {
        this.minSize = minSize;
    }

    public double getMaxSize() {
        return maxSize;
    }

    public void setMaxSize(double maxSize) {
        this.maxSize = maxSize;
    }

    public boolean isParticlesAreWhite() {
        return particlesAreWhite;
    }

    public void setParticlesAreWhite(boolean particlesAreWhite) {
        this.particlesAreWhite = particlesAreWhite;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public float getResolution() {
        return resolution;
    }

    public void setResolution(float resolution) {
        this.resolution = resolution;
    }

    public double getCoordinateBaseX() {
        return coordinateBaseX;
    }

    public void setCoordinateBaseX(double coordinateBaseX) {
        this.coordinateBaseX = coordinateBaseX;
    }

    public double getCoordinateBaseY() {
        return coordinateBaseY;
    }

    public void setCoordinateBaseY(double coordinateBaseY) {
        this.coordinateBaseY = coordinateBaseY;
    }

    public boolean isWatershed() {
        return watershed;
    }

    public void setWatershed(boolean watershed) {
        this.watershed = watershed;
    }

    public ImagePlus getOriginalImage() {
        return originalImage;
    }

    public void setOriginalImage(ImagePlus originalImage) {
        this.originalImage = originalImage;
    }
}
