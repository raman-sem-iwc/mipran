# MIPRAN
## Implementation of an open source algorithm for particle recognition and morphological characterisation for microplastic analysis by means of Raman microspectroscopy

This is the repository corresponding our publication in Analytical Methods.
<https://www.doi.org/10.1039/C9AY01245A>

## Download
To download the current version as binary (.jar or .exe) go to the [release](https://gitlab.lrz.de/raman-sem-iwc/mipran/-/releases) page.

The sources can also be found on the [release](https://gitlab.lrz.de/raman-sem-iwc/mipran/-/releases) page or simply downloaded from the repository using git:

    $ git clone https://gitlab.lrz.de/raman-sem-iwc/mipran.git

## Build
The software uses several third party open source librarys which you need to include in order to be able to build the sotware yourself, they can be found in the /lib folder.

## License
MIPRAN is licenced under the MIT Licence, see LICENSE file.

## Third Party Libraries
MIPRAN uses several third party open source libraries, they can be found in the /lib folder together with their respective licenses.

* [ImageJ 1.52](https://wsr.imagej.net/distros/cross-platform/): [Public Domain](https://imagej.net/disclaimer.html)
* [Apache Commons-CLI 1.4](https://commons.apache.org/proper/commons-cli/download_cli.cgi): [Apache Licence 2.0](http://www.apache.org/licenses/LICENSE-2.0)
* [Opencsv 3.9](https://sourceforge.net/projects/opencsv/files/opencsv/3.9/): [Apache Licence 2.0](http://opencsv.sourceforge.net/licenses.html)

